package com.gx.gxbi.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * ⽤户
 * @TableName user
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * ⽤户
昵称
     */
    private String userName;

    /**
     * ⽤户
头像
     */
    private String userAvatar;
    private String email;

    /**
     * ⽤户⻆⾊：user/admin
     */
    private String userRole;

    /**
     * 创建时间
     */
    private Date createTime;

    private  Long count;

    private  String accessKey;

    private  String secretKey;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}