package com.gx.gxbi.bbb;

import com.gx.gxbi.entity.User;

public interface UserCommonService {
    boolean updateUserCount(long userid);
    User getUserByAs(String accessKey,String secretKey);
}
